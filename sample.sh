#!/bin/bash

. /usr/bin/rhts_environment.sh
. /usr/share/beakerlib/beakerlib.sh || . /usr/lib/beakerlib/beakerlib.sh
. ../libs/virtual-networking/bin/tools.sh

rlJournalStart
rlPhaseStartTest
        rlRun "echo 'This is beakerlib test0.'"
        rlRun "echo $BASH_SOURCE"
        rlRun "assertEquals 'soulde equal' 1 1"
        rlRun "echo \"pwd is $(pwd)\""
        rlRun "ls"
rlPhaseEnd
rlJournalEnd
